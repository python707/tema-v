# Programa que procesa un archivo de datos.

nomFile='data.txt'

def creaArchivo():
    archivo=open(nomFile,'w')
    archivo.truncate()
    archivo.close()
#------------------------------------------------------
def guardaItem(item):
    archivo=open(nomFile,'a')
    archivo.write('{:30s},{:3d},{:3.2f}\n'.format(item[0],item[1],item[2]))
    archivo.close()
#-----------------------------------------------------
def agregaItem():
    item=[0,0,0]
    item[0]=input('Proporciona su nombre:')
    item[1]=int(input('Proporciona su edad:'))
    item[2]=float(input('Proporciona tu estatura:'))
    guardaItem(item)
#-----------------------------------------------------
def imprimeArchivo():
    print('Registros en el archivo')
    archivo=open(nomFile,'r')
    reg='r'
    while reg!='':
        reg=archivo.readline()
        if reg!='':
            l=reg.split(',')
            print('{0:30s}|{1:3s}|{2:5s}'.format(l[0],l[1],l[2]),end='')
    archivo.close()
#------------------------------------------------------
def menu():
    print('1.- Agregar Persona')
    print('2.- Imprimir archivo')
    print('3.- Crear nuevamente el archivo')
    print('4.- Salir')
    opc=int(input('Indica opción:[1..4]:'))
    return opc
#------------------------------------------------------
def principal():
    opc=0
    while opc!=4:
        opc=menu()
        if opc==1:
            agregaItem()
        elif opc==2:
            imprimeArchivo()
        elif opc==3:
            creaArchivo()
#------------------------------------------------------
if __name__=="__main__":
    principal()

    
