# Programa que gestiona un set de tuplas de datos


productos=set()

def capturaProducto():
    id=int(input('Proporciona id de producto:'))
    descripcion=input('Proporciona su descripcion:')
    precio=float(input('Proporciona su precio:'))
    unidades=int(input('Proporciona las existencias:'))
    return (id,descripcion,precio,unidades)


def agregaProducto():
    productos.add(capturaProducto())


def buscarD():
    descripcion=input('Proporciona descripción a buscar:')
    for p in productos:
        if p[1]==descripcion:
            print(p)
            break
    input('Presiona <enter> para continuar...')

def imprimeInversion():
    for p in productos:
        print('{0:03d} {1:30s} {2:07.2f} {3:4d}'.format(p[0],p[1],p[2],p[3]))
    input('Presiona <enter> para continuar...')
    

def menu():
    print('1.- Agregar Producto')
    print('2.- Buscar producto')
    print('3.- Imprimir inversion')
    print('4.- Salir')
    opc=int(input('Indica opción:[1..4]:'))
    return opc

def principal():
    opc=0
    while opc!=4:
        opc=menu()
        if opc==1:
            agregaProducto()
        elif opc==2:
            buscarD()
        elif opc==3:
            imprimeInversion()
            
    



