# Programa que procesa diccionario y lista.

from random import randint

lista=None
diccionario={}
#--------------------------------------------------
def leerArchivo():
    global lista
    f=open('words.txt')
    texto=f.read()
    f.close()
    lista=texto.split()
#--------------------------------------------------
def agregaPalabra():
    global diccionario
    ingles=''
    while ingles=='':
        ingles=lista[randint(0,len(lista)-1)]
        if diccionario.get(ingles)!=None:
            ingles=''     
    palabra=input('Proporciona traducción en español de:'+ingles+" o exit si no sabes:")
    if palabra!='exit':
       if diccionario.get(ingles)==None:
            diccionario[ingles]=palabra
       elif diccionario[ingles]==palabra:
            print('Has ya agregado dicha traducción')
       else:
            print('Ya tiene traducción y no coincide con la que acabas de indicar')
#-----------------------------------------------------
def imprimeDiccionario():
    print('Según lo que encontraste es')
    print('===========================')
    for dic in diccionario:
        print(dic,diccionario[dic],sep='==>')
#----------------------------------------------------
def principal():
    resp='s'
    leerArchivo()
    while resp=='s':
        agregaPalabra()
        resp=input('Desea probar con otra palabra:[s/n]:')
    imprimeDiccionario()

#------------------------------------------------------
if __name__=="__main__":
    principal()

    
